import React from 'react';
import './App.scss';
import { LoginPage } from './components/pages/LoginPage';

function App() {
  return (
    <div className="App">
      <LoginPage></LoginPage>
    </div>
  );
}

export default App;
