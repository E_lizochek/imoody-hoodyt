import './Button.scss'

type Props = {
    onClick?: () => {};
    title: string;
} 

export function Button(props: Props) {

    return (
      <button className='button' onClick={props.onClick} 
      >{props.title}</button>
    );
  }