import { Input } from '../Input/Input';
import { Button } from '../Button/Button';
import './LoginPage.scss'

export function LoginPage() {
    return (
        <div className="lcontainer">
            <div> Вход в личный кабинет </div>
            <Input name="Логин" type='text' placeholder="Введите email" />
            <Input name='Пароль' type='password' placeholder="Введите пароль" />
            <div className='buttonbox'>
                <Button title='Войти' />
                <a> Ещё нет аккаунта </a>
                </div>

                </div>
                )
}