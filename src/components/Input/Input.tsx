import './Input.scss'

type Props = {
    name?: string;
    type: 'password' | 'text';
    placeholder?: string;
}

export function Input(props: Props): JSX.Element {

    return (
        <input className='input' name={props.name} type={props.type} placeholder={props.placeholder}  />
    )
}

